import * as IronSession from 'iron-session'
import { SiweMessage } from 'siwe'

interface LocalUserSession {
  siwe: Partial<SiweMessage>
  signature: string
}

declare module 'iron-session' {
  interface IronSessionData {
    keyring: LocalUserSession[]
    nonce?: string
  }
}

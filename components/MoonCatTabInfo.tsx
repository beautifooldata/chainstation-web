import React from 'react'
import EthereumAddress from './EthereumAddress'
import { TabProps } from '../pages/mooncats/[id]'
import { MoonCatDetails } from 'lib/types'
import { API_SERVER_ROOT, ADDRESS_DETAILS, getMoonCatTidbits } from '../lib/util'
import Icon from './Icon'
import Link from 'next/link'

interface TidbitDefinition {
  label: string
  detail: string
}
const tidbitDetails: { [key: string]: TidbitDefinition } = {
  'numericId': {
    label: 'Numeric ID',
    detail: 'This MoonCat has an identifier that contains only numbers',
  },
  'alphaId': {
    label: 'Alpha ID',
    detail: 'This MoonCat has an identifier that contains only letters',
  },
  'repeatId': {
    label: 'Repeat ID',
    detail: 'This MoonCat has an identifier that has a run of repeated characters at least 3 characters long',
  },
  'firstDay': {
    label: 'Day-One Rescue',
    detail: 'This MoonCat was rescued on the first day of the Rescue',
  },
  'firstWeek': {
    label: 'Week-One Rescue',
    detail: 'This MoonCat was rescued in the first week of the Rescue',
  },
  'yearStart': {
    label: 'New Year',
    detail: 'This MoonCat was the first rescued in an individual calendar year',
  },
  'yearEnd': {
    label: 'Year End',
    detail: 'This MoonCat was the last rescued in an individual calendar year',
  },
  'trueColor': {
    label: 'True Color',
    detail: 'This MoonCat has a coat that is right in the middle of that hue definition',
  },
  'edgeColor': {
    label: 'Edge Color',
    detail: 'This MoonCat has a coat that is very nearly another color label',
  },
  'darkGlow': {
    label: 'Dark Glow',
    detail: 'This MoonCat has a glow that is so dark it appears almost black',
  },
  'brightGlow': {
    label: 'Bright Glow',
    detail: 'This MoonCat has a glow that is so bright it appears almost white',
  },
}

function ucFirst(str: string) {
  return str.charAt(0).toUpperCase() + str.slice(1)
}

const LitterView = ({ details }: { details: MoonCatDetails }) => {
  if (details.onlyChild) {
    return (
      <p>
        This MoonCat is an only child (they have no &ldquo;littermates&rdquo;; MoonCats who have the exact same{' '}
        {details.hue} color (in the {details.hueValue}&deg; hue range) and pattern as them).
      </p>
    )
  }

  return (
    <>
      <h2>Litter</h2>
      <p>
        This MoonCat&rsquo;s litter (MoonCats who share the exact same {details.hue} color (in the {details.hueValue}
        &deg; hue range) and pattern) has {details.litterSize} MoonCats in it:
      </p>
      <div id="item-grid" style={{ margin: '1em 0' }}>
        {details.litter.map((rescueIndex) => {
          if (rescueIndex == details.rescueIndex) {
            return (
              <div key={rescueIndex} className="item-thumb highlight">
                <a href="#">
                  <div
                    className="thumb-img"
                    style={{ backgroundImage: `url(${API_SERVER_ROOT}/image/${rescueIndex}?scale=3&padding=5)` }}
                  />
                  <p>#{rescueIndex}</p>
                  <p>
                    <em>(This MoonCat)</em>
                  </p>
                </a>
              </div>
            )
          }
          let tag: React.ReactNode
          if (details.cloneSet.includes(rescueIndex)) {
            tag = (
              <p>
                <em>Clone</em>
              </p>
            )
          } else if (details.mirrorSet.includes(rescueIndex)) {
            tag = (
              <p>
                <em>Mirror</em>
              </p>
            )
          } else if (details.twinSet.includes(rescueIndex)) {
            tag = (
              <p>
                <em>Twin</em>
              </p>
            )
          }
          return (
            <div key={rescueIndex} className="item-thumb">
              <a href={'/mooncats/' + rescueIndex}>
                <div
                  className="thumb-img"
                  style={{ backgroundImage: `url(${API_SERVER_ROOT}/image/${rescueIndex}?scale=3&padding=5)` }}
                />
                <p>#{rescueIndex}</p>
                {tag}
              </a>
            </div>
          )
        })}
      </div>
    </>
  )
}

const MoonCatTabInfo = ({ moonCat, details }: TabProps) => {
  const owner = details.owner

  let marketplaceDetails: React.ReactNode
  if (details.isAcclimated) {
    marketplaceDetails = (
      <p>
        <a
          target="_blank"
          rel="noreferrer"
          href={`https://app.uniswap.org/#/nfts/asset/0xc3f733ca98e0dad0386979eb96fb1722a1a05e69/${moonCat.rescueOrder}`}
        >
          View this MoonCat on NFT marketplaces
          <Icon name="link-external" style={{ marginLeft: '0.5em', verticalAlign: '-0.2em' }} />
        </a>
      </p>
    )
  } else {
    marketplaceDetails = (
      <p>
        This MoonCat is currently held in the original MoonCatRescue contract, and therefore won&rsquo;t show up on most
        NFT marketplaces. Their owner might wrap them into an ERC721 token (&ldquo;Acclimate&rdquo; them) in the future,
        and after that point they would be visible in tools that enumerate ERC721 tokens.
      </p>
    )
  }

  let coatDetail = `${ucFirst(details.hue)} ${ucFirst(moonCat.pattern)}`
  if (!details.genesis && details.isPale) coatDetail = 'Pale ' + coatDetail

  let nameDetails: React.ReactNode
  if (!!moonCat.name) {
    if (moonCat.name === true) {
      nameDetails = (
        <>
          This MoonCat is named <code>{moonCat.nameRaw}</code> (a raw bit of data; not a valid string name).
        </>
      )
    } else if (moonCat.name == '\ufffd') {
      nameDetails = <>This MoonCat is named, but we have elected not to display that name here.</>
    } else {
      nameDetails = (
        <>
          This MoonCat is named <em>{moonCat.name.trim()}</em>.
        </>
      )
    }
  } else {
    nameDetails = <>This MoonCat doesn&rsquo;t have a name yet. It&rsquo;s owner can choose to give it a name.</>
  }

  let genesisDetails
  if (details.genesis) {
    genesisDetails = (
      <p className="highlight">
        This is a <em>Genesis</em> MoonCat! These sort of MoonCats watch over all the other MoonCats. They weren&rsquo;t
        rescued by Ethereans like the other MoonCats, but willingly joined with the Ethereans in the rescue efforts.
        Genesis MoonCats joined the rescue operation in groups of 16 (this one was part of group {details.genesisGroup}
        ).
      </p>
    )
  }

  let ownerDetails: React.ReactNode
  if (!details.rescuedBy) {
    // This should only happen for Genesis MoonCats.
    if (owner && ADDRESS_DETAILS[owner]?.type == 'pool') {
      // This MoonCat is in an NFT pool address
      const poolDetails = ADDRESS_DETAILS[owner]
      ownerDetails = (
        <>
          This MoonCat is in the <Link href={`/owners/${owner}`}>{poolDetails.label} pool</Link>; you could visit{' '}
          <Link href={poolDetails.link} target="_blank" rel="noreferrer">
            that pool&rsquo;s website
          </Link>{' '}
          and adopt them right now!
        </>
      )
    } else if (owner) {
      ownerDetails = (
        <>
          This MoonCat is currently adopted by <EthereumAddress address={owner} href={`/owners/${owner}`} />.
        </>
      )
    }
  } else if (owner == details.rescuedBy) {
    // MoonCat is in the same address that rescued them.
    ownerDetails = (
      <>
        This MoonCat was rescued by{' '}
        <EthereumAddress address={details.rescuedBy} href={`/owners/${details.rescuedBy}`} />, and is still owned by
        that wallet.
      </>
    )
  } else {
    // MoonCat was rescued by one address, and currently owned by another; the most common situation.
    if (owner && ADDRESS_DETAILS[owner]?.type == 'pool') {
      // This MoonCat is in an NFT pool address
      const poolDetails = ADDRESS_DETAILS[owner]
      ownerDetails = (
        <>
          This MoonCat was originally rescued by{' '}
          <EthereumAddress address={details.rescuedBy} href={`/owners/${details.rescuedBy}`} />, but currently is in the{' '}
          <em>{poolDetails.label}</em>; you could visit{' '}
          <a href={poolDetails.link} target="_blank" rel="noreferrer">
            that pool&rsquo;s website
          </a>{' '}
          and adopt them right now! Or browse <Link href={`/owners/${owner}`}>that pool&rsquo;s collection</Link> here
          on this site.
        </>
      )
    } else if (owner) {
      ownerDetails = (
        <>
          This MoonCat was originally rescued by{' '}
          <EthereumAddress address={details.rescuedBy} href={`/owners/${details.rescuedBy}`} />, but has currently been
          adopted by <EthereumAddress address={owner} href={`/owners/${owner}`} />.
        </>
      )
    } else {
      ownerDetails = (
        <>
          This MoonCat was originally rescued by{' '}
          <EthereumAddress address={details.rescuedBy} href={`/owners/${details.rescuedBy}`} />, but is currently in an
          unsupported wrapper contract.
        </>
      )
    }
  }

  const tidbits = getMoonCatTidbits(moonCat)
  let tidbitsList = []
  for (let key in tidbitDetails) {
    if (tidbits[key]) {
      let detailView
      if (tidbits[key] !== true) {
        detailView = `(${tidbits[key]})`
      }
      tidbitsList.push(
        <li key={key}>
          <strong>{tidbitDetails[key].label}</strong> {tidbitDetails[key].detail} {detailView}
        </li>
      )
    }
  }

  let tidbitsView
  if (tidbitsList.length > 0) {
    tidbitsView = (
      <>
        <h2>Interesting Tidbits</h2>
        <ul>{tidbitsList}</ul>
      </>
    )
  }

  return (
    <div className="text-container">
      <section className="card-help">
        <p>
          This MoonCat has an identifier of <code>{moonCat.catId}</code>. This hexadecimal number is a bit like DNA in
          that it contains compressed information about this MoonCat&rsquo;s basic traits (color, facing, epression,
          pattern, and pose). Other traits have been gleaned by this MoonCat over time, as they and their human owner
          leave their mark on the blockchain.
        </p>
        {marketplaceDetails}
      </section>
      <section className="card">
        <ul style={{ margin: '0 0 1em' }} className="two-col">
          <li>
            <strong>Rescue Order</strong> #{moonCat.rescueOrder}
          </li>
          {typeof moonCat.namedOrder != 'undefined' && (
            <li>
              <strong>Named Order</strong> #{moonCat.namedOrder}
            </li>
          )}
          <li>
            <strong>Identifier</strong> {moonCat.catId}
          </li>
          <li>
            <strong>Facing</strong> {ucFirst(moonCat.facing)}
          </li>
          <li>
            <strong>Expression</strong> {ucFirst(moonCat.expression)}
          </li>
          <li>
            <strong>Coat</strong> {coatDetail}
          </li>
          <li>
            <strong>Pose</strong> {ucFirst(moonCat.pose)}
          </li>
          <li>
            <strong>Rescue Year</strong> {moonCat.rescueYear}
          </li>
          {moonCat.namedYear && (
            <li>
              <strong>Named Year</strong> {moonCat.namedYear}
            </li>
          )}
        </ul>
        {genesisDetails}
        <p>
          {nameDetails} Once a MoonCat is named, it cannot be changed, and will be part of that MoonCat&rsquo;s traits
          forever after.
        </p>
        <p>{ownerDetails}</p>
        <LitterView details={details} />
        {tidbitsView}
      </section>
    </div>
  )
}
export default MoonCatTabInfo

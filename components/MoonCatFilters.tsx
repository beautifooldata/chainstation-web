import React, { CSSProperties, ChangeEventHandler, useCallback, useEffect, useState } from 'react'
import { MoonCatFilterSettings } from 'lib/types'
import { interleave } from 'lib/util'

/**
 * Define the structure of the filters to assemble into a form
 */
interface SelectFieldMeta {
  label: string
  options: Record<string, string>
}
interface TextFieldMeta {
  label: string
}

function isSelectFieldMeta(m: SelectFieldMeta | TextFieldMeta): m is SelectFieldMeta {
  return typeof (m as SelectFieldMeta).options != 'undefined'
}

const filterMeta: Record<keyof MoonCatFilterSettings, SelectFieldMeta | TextFieldMeta> = {
  classification: {
    label: 'Classification',
    options: {
      genesis: 'Genesis',
      rescue: 'Rescue',
    },
  } as SelectFieldMeta,
  hue: {
    label: 'Hue',
    options: {
      white: 'White',
      black: 'Black',
      red: 'Red',
      orange: 'Orange',
      yellow: 'Yellow',
      chartreuse: 'Chartreuse',
      green: 'Green',
      teal: 'Teal',
      cyan: 'Cyan',
      skyblue: 'SkyBlue',
      blue: 'Blue',
      purple: 'Purple',
      magenta: 'Magenta',
      fuchsia: 'Fuchsia',
    },
  } as SelectFieldMeta,
  pale: {
    label: 'Pale-Colored',
    options: {
      no: 'No',
      yes: 'Yes',
    },
  } as SelectFieldMeta,
  facing: {
    label: 'Facing',
    options: {
      right: 'Right',
      left: 'Left',
    },
  } as SelectFieldMeta,
  expression: {
    label: 'Expression',
    options: {
      smiling: 'Smiling',
      grumpy: 'Grumpy',
      pouting: 'Pouting',
      shy: 'Shy',
    },
  } as SelectFieldMeta,
  pattern: {
    label: 'Coat Pattern',
    options: {
      pure: 'Pure',
      tabby: 'Tabby',
      spotted: 'Spotted',
      tortie: 'Tortie',
    },
  } as SelectFieldMeta,
  pose: {
    label: 'Pose',
    options: {
      standing: 'Standing',
      pouncing: 'Pouncing',
      stalking: 'Stalking',
      sleeping: 'Sleeping',
    },
  } as SelectFieldMeta,
  rescueYear: {
    label: 'Rescue Year',
    options: {
      2017: '2017',
      2018: '2018',
      2019: '2019',
      2020: '2020',
      2021: '2021',
    },
  } as SelectFieldMeta,
  named: {
    label: 'Named',
    options: {
      no: 'Not Named',
      yes: 'Named',
      valid: 'String Names',
      invalid: 'Other Names',
    },
  } as SelectFieldMeta,
  nameKeyword: {
    label: 'Name Keyword',
  } as TextFieldMeta,
}
type FieldId = keyof typeof filterMeta

/**
 * Render an individual form field, using a HTML SELECT element as the control
 */
interface FieldProps {
  fieldId: FieldId
  labelStyle: CSSProperties
  currentValue: any
  onChange: ChangeEventHandler<HTMLElement>
}
const SelectField = ({ fieldId, labelStyle, currentValue, onChange }: FieldProps) => {
  const meta = filterMeta[fieldId] as SelectFieldMeta
  return (
    <div className="field-row">
      <div className="field-label" style={labelStyle}>
        {meta.label}
      </div>
      <div className="field-control">
        <select name={fieldId} value={currentValue} onChange={onChange}>
          <option value="">Any</option>
          {Object.keys(meta.options).map((optionValue) => {
            return (
              <option key={optionValue} value={optionValue}>
                {meta.options[optionValue]}
              </option>
            )
          })}
        </select>
      </div>
    </div>
  )
}

const TextField = ({ fieldId, labelStyle, currentValue, onChange }: FieldProps) => {
  const meta = filterMeta[fieldId] as TextFieldMeta
  return (
    <div className="field-row">
      <div className="field-label" style={labelStyle}>
        {meta.label}
      </div>
      <div className="field-control">
        <input type="text" name={fieldId} value={currentValue} onChange={onChange} />
      </div>
    </div>
  )
}

interface Props {
  id?: string
  currentFilters: MoonCatFilterSettings
  filteredCount: number
  totalCount: number
  onChange: (propName: keyof MoonCatFilterSettings, newValue: any) => void
}
const MoonCatFilters = ({ id, currentFilters, filteredCount, totalCount, onChange }: Props) => {
  const [isOpen, setOpen] = useState<boolean>(false)

  /**
   * Handle a change to one of the form elements
   */
  function handleChange(e: any) {
    const propName = e.target.name as keyof MoonCatFilterSettings
    const newValue = e.target.value
    onChange(propName, newValue)
  }

  /**
   * Set the visibility of the full filters form to the opposite of what it currently is
   */
  function toggleModal() {
    setOpen(!isOpen)
  }

  /**
   * Document-level event listener
   * For every key press, if it was the Escape key, close the filters form
   */
  const escListener = useCallback(
    (event: any) => {
      if (event.key == 'Escape') {
        setOpen(false)
      }
    },
    [setOpen]
  )

  /**
   * On mount, start listening for keypresses
   */
  useEffect(() => {
    document.addEventListener('keydown', escListener, false)
    return () => {
      document.removeEventListener('keydown', escListener, false)
    }
  }, [escListener])

  // Build UI labels for the current filter state
  let filterPhrases = []
  for (let [propName, currentValue] of Object.entries(currentFilters)) {
    const filterName = propName as keyof MoonCatFilterSettings
    const meta = filterMeta[filterName]
    if (!!currentValue) {
      const displayValue = isSelectFieldMeta(meta) ? (meta as SelectFieldMeta).options[currentValue] : currentValue
      filterPhrases.push(
        <React.Fragment key={filterName}>
          {meta.label}: <em>{displayValue}</em>
        </React.Fragment>
      )
    }
  }
  let filterPhrase: Array<JSX.Element | string> = ['']
  if (filterPhrases.length > 0) {
    filterPhrase = interleave(filterPhrases, ', ')
    filterPhrase.push('. ')
  }
  let filterSize: React.ReactNode
  if (filteredCount < totalCount) {
    filterSize = `${filteredCount.toLocaleString()} of ${totalCount.toLocaleString()} MoonCats`
  } else if (totalCount == 1) {
    // Showing just one MoonCat? No label for this
  } else if (totalCount == 2) {
    filterSize = `Showing both MoonCats`
  } else if (totalCount > 0) {
    filterSize = `Showing all ${totalCount.toLocaleString()} MoonCats`
  }

  const modalStyle = isOpen ? {} : { display: 'none' }
  const labelStyle = { width: '200px' }
  return (
    <div id={id} style={{ position: 'relative', display: 'flex', alignItems: 'baseline' }}>
      <button onClick={toggleModal}>Filter</button>
      <div style={{ padding: '0 1rem', fontSize: '0.8rem' }}>
        {filterPhrase}
        {filterSize}
      </div>
      <div className="filter-modal" style={modalStyle}>
        <SelectField
          fieldId="classification"
          labelStyle={labelStyle}
          currentValue={currentFilters.classification}
          onChange={handleChange}
        />
        <SelectField fieldId="hue" labelStyle={labelStyle} currentValue={currentFilters.hue} onChange={handleChange} />
        <SelectField
          fieldId="pale"
          labelStyle={labelStyle}
          currentValue={currentFilters.pale}
          onChange={handleChange}
        />
        <SelectField
          fieldId="facing"
          labelStyle={labelStyle}
          currentValue={currentFilters.facing}
          onChange={handleChange}
        />
        <SelectField
          fieldId="expression"
          labelStyle={labelStyle}
          currentValue={currentFilters.expression}
          onChange={handleChange}
        />
        <SelectField
          fieldId="pattern"
          labelStyle={labelStyle}
          currentValue={currentFilters.pattern}
          onChange={handleChange}
        />
        <SelectField
          fieldId="pose"
          labelStyle={labelStyle}
          currentValue={currentFilters.pose}
          onChange={handleChange}
        />
        <SelectField
          fieldId="rescueYear"
          labelStyle={labelStyle}
          currentValue={currentFilters.rescueYear}
          onChange={handleChange}
        />
        <SelectField
          fieldId="named"
          labelStyle={labelStyle}
          currentValue={currentFilters.named}
          onChange={handleChange}
        />
        <TextField
          fieldId="nameKeyword"
          labelStyle={labelStyle}
          currentValue={currentFilters.nameKeyword}
          onChange={handleChange}
        />
      </div>
    </div>
  )
}
export default MoonCatFilters

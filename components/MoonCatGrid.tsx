import React, { useCallback, useEffect, useState } from 'react'
import MoonCatThumb from './MoonCatThumb'
import MoonCatFilters from './MoonCatFilters'
import MoonCatViewSelector from './MoonCatViewSelector'
import Pagination from './Pagination'
import { MoonCatData, MoonCatFilterSettings } from '../lib/types'
import { useIsMounted, useFetchStatus } from '../lib/util'

const DEFAULT_PER_PAGE = 50

interface FilterParams {
  pageNumber: number
  perPage: number
  moonCats: number[] | 'all'
  filters: MoonCatFilterSettings
}

// Custom hook to encapsulate fetching data from local API endpoint
const useMoonCatPage = (apiPage: string) => {
  const [status, setStatus] = useFetchStatus()
  const [moonCatPage, setMoonCatPage] = useState<MoonCatData[]>([])
  const [filteredSetSize, setFilteredSetSize] = useState<number>(0)
  const [totalMoonCats, setTotalMoonCats] = useState<number>(-1)

  // Call the local API endpoint to fetch a page of results
  const doFilter = useCallback(
    async function ({ pageNumber, perPage, moonCats, filters }: FilterParams) {
      setStatus('pending')
      let params = new URLSearchParams(filters as Record<string, string>)
      params.set('limit', String(perPage!))
      params.set('offset', String(pageNumber * perPage!))
      if (Array.isArray(moonCats)) {
        params.set('mooncats', moonCats.join(','))
      } else {
        params.set('mooncats', moonCats)
      }
      try {
        let searchRes = await fetch(`/api/${apiPage}?${params.toString()}`)
        let data = await searchRes.json()
        setFilteredSetSize(data.length)
        setTotalMoonCats(data.totalLength)
        setMoonCatPage(data.moonCats)
        setStatus('done')
      } catch (err) {
        console.error(err)
        setStatus('error')
      }
    },
    [apiPage, setStatus]
  )

  return {
    status,
    moonCatPage,
    filteredSetSize,
    totalMoonCats,
    doFilter,
  }
}

interface Props {
  children?: Parameters<typeof MoonCatThumb>[0]['thumbHandler']
  moonCats: number[] | 'all'
  perPage?: number
  apiPage: string
  isEventActive?: boolean
}

const MoonCatGrid = ({
  children: thumbHandler,
  moonCats: allMoonCats,
  perPage,
  apiPage,
  isEventActive = false,
}: Props) => {
  const isMounted = useIsMounted()
  if (!perPage) {
    perPage = DEFAULT_PER_PAGE
  }
  const [filterProps, setFilterProps] = useState<FilterParams>({
    pageNumber: 0,
    perPage: perPage,
    moonCats: allMoonCats,
    filters: {},
  })
  const { status, moonCatPage, filteredSetSize, totalMoonCats, doFilter } = useMoonCatPage(apiPage)

  // Run once on mount, to fetch any existing preferences from the URL
  useEffect(() => {
    // Set currently-viewed page of data
    const params = new URLSearchParams(window.location.search)
    const pagePreference = params.get('page')
    if (pagePreference !== null) {
      let page = parseInt(pagePreference)
      if (page > 0) {
        // Page is valid; jump to it
        console.debug('Jumping to page:', page)
        setFilterProps((curProps) => {
          return { ...curProps, pageNumber: page - 1 }
        })
      }
    }
  }, [])

  // Event handler for updates from MoonCatFilter component when a user picks a new filter value
  function handleFilterUpdate(prop: keyof MoonCatFilterSettings, newValue: any) {
    setFilterProps((curProps) => {
      let newFilters = Object.assign({}, curProps.filters)
      if (newValue == '') {
        if (!curProps.filters[prop]) {
          // Already blank
          return curProps
        }
        delete newFilters[prop]
      } else {
        if (curProps.filters[prop] == newValue) {
          // Already set to that value
          return curProps
        }
        newFilters[prop] = newValue
      }

      return { ...curProps, filters: newFilters, pageNumber: 0 }
    })
  }

  // Event handler for updates from Pagination component when user navigates to a new page
  function handlePageUpdate(newPage: number) {
    setFilterProps((curProps) => {
      if (curProps.pageNumber == newPage) {
        // Already set to that value
        return curProps
      }

      return { ...curProps, pageNumber: newPage }
    })
  }

  // When filter parameters change, update URL and fetch updated page of MoonCats
  useEffect(() => {
    if (!isMounted) return // Skip on mount; only needed on update
    const params = new URLSearchParams(window.location.search)
    params.set('page', String(filterProps.pageNumber + 1)) // pageNumber is zero-based, but we show it one-based for human-friendliness
    window.history.replaceState({}, '', `${window.location.pathname}?${params}`)
    console.log('filtering again')
    doFilter(filterProps)
  }, [isMounted, filterProps, doFilter])

  return (
    <div className="mooncat-grid">
      <div
        style={{
          display: 'flex',
          alignItems: 'baseline',
          padding: '1rem 0',
        }}
        className="text-scrim"
      >
        {totalMoonCats > 1 && (
          <MoonCatFilters
            id="filters"
            currentFilters={filterProps.filters}
            filteredCount={filteredSetSize}
            totalCount={totalMoonCats}
            onChange={handleFilterUpdate}
          />
        )}
        <div style={{ flexGrow: 10 }} />
        <MoonCatViewSelector isEventActive={isEventActive} />
      </div>
      <div id="item-grid" style={{ margin: '0 0 2rem' }}>
        {moonCatPage.map((mc) => (
          <MoonCatThumb key={mc.rescueOrder} moonCat={mc} thumbHandler={thumbHandler} />
        ))}
      </div>
      <Pagination
        currentPage={filterProps.pageNumber}
        maxPage={Math.ceil(filteredSetSize / perPage) - 1}
        setCurrentPage={handlePageUpdate}
      />
    </div>
  )
}
export default MoonCatGrid

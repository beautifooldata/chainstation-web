import { MoonCatViewPreference } from 'lib/types'
import { AppGlobalActionType, AppGlobalViewAction, AppVisitorContext } from 'lib/util'
import React, { useContext } from 'react'

interface Props {
  isEventActive: boolean
}

const MoonCatViewSelector = ({ isEventActive }: Props) => {
  const {
    state: { viewPreference },
    dispatch,
  } = useContext(AppVisitorContext)
  function handleOnChange(e: React.ChangeEvent<HTMLSelectElement>) {
    if (dispatch)
      dispatch({
        type: AppGlobalActionType.SET_VIEW_PREFERENCE,
        payload: e.target.value as MoonCatViewPreference,
      } as AppGlobalViewAction)
  }
  return (
    <div>
      <label style={{ fontSize: '0.8rem' }}>
        View Style:
        <select value={viewPreference as string} onChange={handleOnChange} style={{ marginLeft: '0.5rem' }}>
          {isEventActive && <option value="event">Current Event</option>}
          <option value="accessorized">Accessorized</option>
          <option value="mooncat">MoonCat</option>
          <option value="face">MoonCat Face</option>
        </select>
      </label>
    </div>
  )
}
export default MoonCatViewSelector

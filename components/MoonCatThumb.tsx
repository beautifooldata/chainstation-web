import React, { useContext } from 'react'
import { useRouter } from 'next/router'
import Icon from './Icon'
import { API_SERVER_ROOT, AppVisitorContext } from '../lib/util'
import { MoonCatData } from '../lib/types'

interface ThumbProps {
  moonCat: MoonCatData
  className?: string
  thumbHandler?: (moonCat: MoonCatData) => React.ReactNode
  onClick?: (moonCat: MoonCatData, event: React.MouseEvent<HTMLElement, MouseEvent>) => any
}

const MoonCatThumb = ({ moonCat, className, thumbHandler, onClick }: ThumbProps) => {
  const router = useRouter()
  const {
    state: { viewPreference },
  } = useContext(AppVisitorContext)

  let thumbStyle: React.CSSProperties = {}
  switch (viewPreference) {
    case 'accessorized':
      thumbStyle.backgroundImage = `url(${API_SERVER_ROOT}/image/${moonCat.rescueOrder}?scale=3&padding=5)`
      break
    case 'mooncat':
      thumbStyle.backgroundImage = `url(${API_SERVER_ROOT}/image/${moonCat.rescueOrder}?scale=3&padding=5&acc=)`
      break
    case 'face':
      thumbStyle.backgroundImage = `url(${API_SERVER_ROOT}/image/${moonCat.rescueOrder}?scale=3&padding=5&acc=&headOnly)`
      break
    case 'event':
      thumbStyle.backgroundImage = `url(${API_SERVER_ROOT}/event-image/${moonCat.rescueOrder}?scale=3&padding=5)`
      break
  }

  let details: React.ReactNode
  if (thumbHandler) {
    details = thumbHandler(moonCat)
  } else {
    let name = null
    if (typeof moonCat.name != 'undefined') {
      if (moonCat.name === true || moonCat.name == '\ufffd') {
        name = (
          <p>
            <Icon name="question" />
          </p>
        )
      } else {
        name = <p>{moonCat.name.trim()}</p>
      }
    }

    details = (
      <>
        <p>#{moonCat.rescueOrder}</p>
        <p>
          <code>{moonCat.catId}</code>
        </p>
        {name}
      </>
    )
  }

  const defaultClickHandler = (mc: MoonCatData, event: React.MouseEvent<HTMLElement, MouseEvent>) => {
    const uri = `/mooncats/${mc.rescueOrder}`
    if (event.ctrlKey || event.metaKey) {
      window.open(uri)
    } else {
      router.push(uri)
    }
  }

  const clickHandler = typeof onClick == 'function' ? onClick : defaultClickHandler

  let classes = ['item-thumb']
  if (className) classes.push(className)

  return (
    <div className={classes.join(' ')} onClick={(e) => clickHandler(moonCat, e)}>
      <div className="thumb-img" style={thumbStyle} />
      {details}
    </div>
  )
}
export default MoonCatThumb

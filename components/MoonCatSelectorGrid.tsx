import React, { useCallback, useEffect, useState } from 'react'
import MoonCatThumb from './MoonCatThumb'
import Pagination from './Pagination'
import { MoonCatData, MoonCatFilterSettings } from '../lib/types'
import { useIsMounted, useFetchStatus } from '../lib/util'

const DEFAULT_PER_PAGE = 50

interface FilterParams {
  pageNumber: number
  perPage: number
  moonCats: number[] | 'all'
  filters: MoonCatFilterSettings
}

// Custom hook to encapsulate fetching data from local API endpoint
const useMoonCatPage = (apiPage: string) => {
  const [status, setStatus] = useFetchStatus()
  const [moonCatPage, setMoonCatPage] = useState<MoonCatData[]>([])
  const [filteredSetSize, setFilteredSetSize] = useState<number>(0)
  const [totalMoonCats, setTotalMoonCats] = useState<number>(-1)

  // Call the local API endpoint to fetch a page of results
  const doFilter = useCallback(
    async function ({ pageNumber, perPage, moonCats, filters }: FilterParams) {
      setStatus('pending')
      let params = new URLSearchParams(filters as Record<string, string>)
      params.set('limit', String(perPage!))
      params.set('offset', String(pageNumber * perPage!))
      if (Array.isArray(moonCats)) {
        params.set('mooncats', moonCats.join(','))
      } else {
        params.set('mooncats', moonCats)
      }
      try {
        let searchRes = await fetch(`/api/${apiPage}?${params.toString()}`)
        let data = await searchRes.json()
        setFilteredSetSize(data.length)
        setTotalMoonCats(data.totalLength)
        setMoonCatPage(data.moonCats)
        setStatus('done')
      } catch (err) {
        console.error(err)
        setStatus('error')
      }
    },
    [apiPage, setStatus]
  )

  return {
    status,
    moonCatPage,
    filteredSetSize,
    totalMoonCats,
    doFilter,
  }
}

interface Props {
  children?: Parameters<typeof MoonCatThumb>[0]['thumbHandler']
  moonCats: number[] | 'all'
  perPage?: number
  apiPage: string
  disabledMoonCats?: number[]
  highlightedMoonCats?: number[]
  onClick?: (moonCat: MoonCatData) => any
  minCellWidth?: string
}

const MoonCatGrid = ({
  children: thumbHandler,
  moonCats: allMoonCats,
  perPage,
  apiPage,
  disabledMoonCats = [],
  highlightedMoonCats = [],
  onClick,
  minCellWidth = '150px',
}: Props) => {
  const isMounted = useIsMounted()
  if (typeof perPage === 'undefined') {
    perPage = DEFAULT_PER_PAGE
  }
  const [filterProps, setFilterProps] = useState<FilterParams>({
    pageNumber: 0,
    perPage: perPage,
    moonCats: allMoonCats,
    filters: {},
  })
  const { status, moonCatPage, filteredSetSize, totalMoonCats, doFilter } = useMoonCatPage(apiPage)

  // When list of MoonCats change, update the filter parameters
  useEffect(() => {
    setFilterProps((current) => {
      let newFilterProps = Object.assign({}, current)
      newFilterProps.moonCats = allMoonCats
      return newFilterProps
    })
  }, [allMoonCats])

  // Event handler for updates from Pagination component when user navigates to a new page
  function handlePageUpdate(newPage: number) {
    setFilterProps((curProps) => {
      if (curProps.pageNumber == newPage) {
        // Already set to that value
        return curProps
      }

      return { ...curProps, pageNumber: newPage }
    })
  }

  // When filter parameters change, fetch updated page of MoonCats
  useEffect(() => {
    if (!isMounted) return // Skip on mount; only needed on update
    doFilter(filterProps)
  }, [isMounted, filterProps, doFilter])

  return (
    <div className="mooncat-grid">
      <div
        id="item-grid"
        style={{ margin: '0 0 2rem', gridTemplateColumns: `repeat(auto-fill, minmax(${minCellWidth}, 1fr))` }}
      >
        {moonCatPage.map((mc) => {
          let classes = []
          let clickHandler = onClick
          if (highlightedMoonCats.includes(mc.rescueOrder)) classes.push('highlight')
          if (disabledMoonCats.includes(mc.rescueOrder)) {
            classes.push('disabled')
            clickHandler = () => {}
          }

          return (
            <MoonCatThumb
              key={mc.rescueOrder}
              className={classes.join(' ')}
              moonCat={mc}
              thumbHandler={thumbHandler}
              onClick={clickHandler}
            />
          )
        })}
      </div>
      <Pagination
        currentPage={filterProps.pageNumber}
        maxPage={Math.ceil(filteredSetSize / perPage) - 1}
        setCurrentPage={handlePageUpdate}
      />
    </div>
  )
}
export default MoonCatGrid

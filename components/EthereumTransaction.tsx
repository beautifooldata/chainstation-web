import React from 'react'
import Link from 'next/link'
import Icon from './Icon'

interface Props {
  hash: `0x${string}`
  href?: string
  mode?: 'card' | 'bar' | 'short-bar' | 'mini'
  className?: string
  style?: React.CSSProperties
}

const EthereumTransaction = ({ hash, href, className, style, mode = 'short-bar' }: Props) => {
  let classes = 'eth-tx eth-tx-' + mode
  if (!!className) classes += ' ' + className

  const iconStyle: React.CSSProperties = { marginLeft: '0.5em', verticalAlign: '-0.2em' }

  switch (mode) {
    case 'bar':
    case 'short-bar':
      let label: React.ReactNode
      if (mode == 'short-bar') {
        label = (
          <span className="tx-label">
            {hash.substring(0, 8)}...{hash.substring(hash.length - 6)}
          </span>
        )
      } else {
        label = <span className="tx-label hex-collapse">{hash}</span>
      }

      let labelView
      if (!!href) {
        labelView = (
          <Link href={href} passHref legacyBehavior>
            <a style={{ lineHeight: '1em' }}>
              {label}
            </a>
          </Link>
        )
      } else {
        labelView = (
          <>
            {label}
          </>
        )
      }

      return (
        <span title={mode == 'short-bar' ? hash : ''} className={classes} style={style}>
          {labelView}
          <a
            title="Copy address"
            className="alt-link"
            style={{ cursor: 'pointer' }}
            onClick={() => {
              navigator.clipboard.writeText(hash)
            }}
          >
            <Icon name="copy" style={iconStyle} />
          </a>
          <a
            href={`https://etherscan.io/tx/${hash}`}
            className="alt-link"
            title="View on Etherscan"
            target="_blank"
            rel="noreferrer"
          >
            <Icon name="link-external" style={iconStyle} />
          </a>
        </span>
      )
    default:
      return <div>Unknown mode</div>
  }
}
export default EthereumTransaction

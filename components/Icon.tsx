import React from 'react'

interface Props {
  name: string
  className?: string
  style?: React.CSSProperties
}

const Icon = ({ name, className, style }: Props) => {
  let classes = `icon icon-${name}`
  if (!!className) {
    classes += ' ' + className
  }
  return (
    <svg className={classes} style={style}>
      <use xlinkHref={`/img/symbol-defs.svg#icon-${name}`}></use>
    </svg>
  )
}
export default Icon

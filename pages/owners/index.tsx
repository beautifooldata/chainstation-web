import React from 'react'
import AddressLookup from 'components/AddressLookup'
import RandomMoonCatRow from 'components/RandomMoonCatRow'
import { ADDRESS_DETAILS } from 'lib/util'
import type { NextPage } from 'next'
import Head from 'next/head'
import { useRouter } from 'next/router'
import Link from 'next/link'
import { Address } from 'wagmi'

const ZWS = '\u200B'

const Owners: NextPage = () => {
  const router = useRouter()

  async function doLookup(address: Address) {
    router.push(`/owners/${address}`)
  }

  const pageTitle = 'Owner Profiles'

  return (
    <div id="content-container">
      <Head>
        <title>{pageTitle}</title>
        <meta property="og:title" content={pageTitle} />
        <meta name="description" property="og:title" content="Gallery view of individual Ethereum address holdings" />
      </Head>
      <div className="text-container">
        <h1 className="hero">Owner Profiles</h1>
        <RandomMoonCatRow />
        <section className="card">
          <AddressLookup style={{ marginBottom: '1em' }} onValidate={doLookup} />
          <p>Enter an Ethereum address (or ENS name) to look up that address&rsquo; information.</p>
        </section>
        <section className="card">
          <h2>NFT Pools</h2>
          <p>
            Not sure where to start browsing? Here&rsquo;s some addresses that are NFT pools, so all the MoonCats in
            them are up for adoption <em>right now</em>!
          </p>
          <ul>
            {Object.entries(ADDRESS_DETAILS)
              .filter(([k, v]) => {
                return v.type == 'pool'
              })
              .map(([k, v]) => {
                return (
                  <li key={k}>
                    <Link href={`/owners/${k}`}>{v.label}</Link>
                  </li>
                )
              })}
          </ul>
        </section>
      </div>
    </div>
  )
}
export default Owners

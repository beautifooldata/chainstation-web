import React from 'react'
import { Address } from 'wagmi'
import type { NextPage } from 'next'
import Head from 'next/head'
import Link from 'next/link'
import { useRouter } from 'next/router'
import AddressLookup from 'components/AddressLookup'
import RandomMoonCatRow from 'components/RandomMoonCatRow'

const ZWS = '\u200B'
const pageTitle = 'MoonCatNameService'
const MoonCatNameService: NextPage = () => {
  const router = useRouter()

  async function doLookup(address: Address) {
    router.push(`/mcns/${address}`)
  }

  return (
    <div id="content-container">
      <Head>
        <title>{pageTitle}</title>
        <meta property="og:title" content={pageTitle} />
        <meta
          name="description"
          property="og:desscription"
          content="Ethereum Name Service (ENS) domains for each MoonCat"
        />
      </Head>
      <div className="text-container">
        <h1 className="hero">MoonCatNameService</h1>
        <section className="card">
          <p>
            MoonCats form close bonds with their Etherian owners, but while both are traveling the digital and physical
            metaverse realms, it can cause some insecurity for the MoonCats to know where their owners are at. To
            rectify this issue, the MoonCats tapped into a galactic beacon network, so their owners would be only a ping
            away!
          </p>
          <p>
            <Link href="/mcns/faq">More details...</Link>
          </p>
        </section>
        <RandomMoonCatRow />
        <section className="card">
          <AddressLookup style={{ marginBottom: '1em' }} onValidate={doLookup} />
          <p>Enter an Ethereum address (or ENS name) to look up that address&rsquo; information.</p>
        </section>
      </div>
    </div>
  )
}
export default MoonCatNameService

import React from 'react'
import type { NextPage } from 'next'
import Head from 'next/head'
import RandomMoonCatRow from 'components/RandomMoonCatRow'

const ZWS = '\u200B'
const pageTitle = 'Thank You!'
const ThankYou: NextPage = () => {
  return (
    <div id="content-container">
      <Head>
        <title>{pageTitle}</title>
        <meta property="og:title" content={pageTitle} />
      </Head>
      <div className="text-container">
        <h1 className="hero">Thank You!</h1>
        <RandomMoonCatRow />
        <section className="card">
          <p>Thanks for believing in us!</p>
          <p>
            As a two-person team, we appreciate all the passion and support - financial and otherwise - our community
            has shown over the years. Big or small, this matters in ensuring MoonCats last through thick and thin.
          </p>
        </section>
      </div>
    </div>
  )
}
export default ThankYou

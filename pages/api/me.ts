import { withIronSessionApiRoute } from 'iron-session/next'
import { NextApiRequest, NextApiResponse } from 'next'
import ironOptions from 'lib/ironOptions'
import { isSessionValid } from 'lib/firebase'
import { SiweMessage } from 'siwe'

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const { method } = req
  const finalize = (rs: SiweMessage['address'][]) => {
    res.send({ addresses: rs })
  }
  switch (method) {
    case 'GET':
      // Currently all activity happens on the main chain
      const targetChain = 1

      const keyring = req.session.keyring
      if (!keyring || !Array.isArray(keyring) || keyring.length == 0) {
        // Keyring is missing/empty
        console.log('api/me: User keyring is empty; nothing to validate')
        return finalize([])
      }

      const toCheck = keyring.filter((k) => k.siwe.chainId == targetChain)
      if (toCheck.length == 0) {
        // No Keys for this chain
        return finalize([])
      }

      let validKeys = []
      for (let k of toCheck) {
        const isValid = await isSessionValid(k.siwe, k.signature)
        if (isValid) {
          validKeys.push(k)
        } else {
          // No verified user session exists; user logged out and a stale session is trying to be used
          console.error('Stale session attempted for', k.siwe.address)
          console.error('forwarded-for', req.headers['x-forwarded-for'])
          console.error('remote address', req.socket.remoteAddress)
        }
      }

      // Replace user's local state with only valid keys
      if (validKeys.length == 0) {
        req.session.destroy()
      } else {
        req.session.keyring = validKeys
        await req.session.save()
      }
      return finalize(validKeys.map((k) => k.siwe.address!))
    case 'OPTIONS':
      res.setHeader('Allow', ['GET'])
      res.send(200)
      return
    default:
      res.setHeader('Allow', ['GET'])
      res.status(405).end(`Method ${method} Not Allowed`)
      return
  }
}

export default withIronSessionApiRoute(handler, ironOptions)

import { withIronSessionApiRoute } from 'iron-session/next'
import { NextApiRequest, NextApiResponse } from 'next'
import ironOptions from 'lib/ironOptions'
import { generateNonce } from 'siwe'

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const { method } = req
  switch (method) {
    case 'GET':
      // On every request, update the expected nonce the user should sign with
      req.session.nonce = generateNonce()

      await req.session.save() // Encrypt the session data and sent it to the user as a cookie to return on future requests
      res.setHeader('Content-Type', 'text/plain')
      res.send(req.session.nonce)
      return
    case 'OPTIONS':
      res.setHeader('Allow', ['GET'])
      res.send(200)
      return
    default:
      res.setHeader('Allow', ['GET'])
      res.status(405).end(`Method ${method} Not Allowed`)
      return
  }
}

export default withIronSessionApiRoute(handler, ironOptions)

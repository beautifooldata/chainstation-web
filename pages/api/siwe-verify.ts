import { Timestamp } from 'firebase-admin/firestore'
import { withIronSessionApiRoute } from 'iron-session/next'
import { NextApiRequest, NextApiResponse } from 'next'
import ironOptions from 'lib/ironOptions'
import { SIWE_LOGIN_STATEMENT, USER_SESSION_COLLECTION } from 'lib/util'
import { getAppFirestore } from 'lib/firebase'
import { SiweMessage, SiweResponse } from 'siwe'

const db = getAppFirestore()

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const { method } = req
  const finalize = (rs: boolean) => {
    res.send({ ok: rs })
  }
  switch (method) {
    case 'POST':
      try {
        const { message, signature } = req.body
        const siweMessage = new SiweMessage(message)
        if (siweMessage.statement != SIWE_LOGIN_STATEMENT) {
          // Not the proper statement to sign
          console.error(`api/siwe-verify: User signed message ${siweMessage.statement}, which is not recognized`)
          return finalize(false)
        }

        let verifyRes: SiweResponse
        try {
          verifyRes = await siweMessage.verify({
            signature,
            nonce: req.session.nonce,
          })
        } catch (err) {
          console.error('api/siwe-verify: SIWE error', (err as SiweResponse).error)
          return finalize(false)
        }
        const verifiedData = verifyRes.data

        // Save session to database, overwriting any existing session for this address, on this chain
        let sessionDoc: FirebaseFirestore.DocumentData = {
          address: verifiedData.address,
          statement: verifiedData.statement,
          nonce: verifiedData.nonce,
          requestId: verifiedData.requestId,
        }
        if (verifiedData.issuedAt) {
          sessionDoc.issuedAt = Timestamp.fromDate(new Date(verifiedData.issuedAt))
        }
        if (verifiedData.expirationTime) {
          sessionDoc.expirationTime = Timestamp.fromDate(new Date(verifiedData.expirationTime))
        }
        if (verifiedData.notBefore) {
          sessionDoc.notBefore = Timestamp.fromDate(new Date(verifiedData.notBefore))
        }
        const sessionKey = `${verifiedData.address}-${verifiedData.chainId}`
        await db.collection(USER_SESSION_COLLECTION).doc(sessionKey).set(sessionDoc)
        console.log(`api/siwe-verify: Database session saved ${sessionKey}`)

        // Save session to user cookie
        if (typeof req.session.keyring == 'undefined' || !Array.isArray(req.session.keyring)) {
          // No keyring yet. Add just this one
          req.session.keyring = [{ siwe: verifiedData, signature: signature }]
        } else {
          // Remove any old session info for this address, on this chain
          req.session.keyring = req.session.keyring.filter(
            (k) => k.siwe.address != verifiedData.address || k.siwe.chainId != verifiedData.chainId
          )
          req.session.keyring.push({
            siwe: verifiedData,
            signature: signature,
          })
        }
        await req.session.save()

        return finalize(true)
      } catch (err) {
        console.error('api/siwe-verify: Unknown error in SIWE flow', err)
        return finalize(false)
      }
    default:
      res.setHeader('Allow', ['POST'])
      res.status(405).end(`Method ${method} Not Allowed`)
  }
}

export default withIronSessionApiRoute(handler, ironOptions)

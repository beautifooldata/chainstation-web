import React, { useEffect, useState } from 'react'
import type { NextPage, GetServerSideProps } from 'next'
import Head from 'next/head'
import Link from 'next/link'
import { MoonCatData } from 'lib/types'
import { API_SERVER_ROOT, useFetchStatus } from 'lib/util'
import EthereumAddress from 'components/EthereumAddress'
import Icon from 'components/Icon'

const ZWS = '\u200B'

interface Props {
  accessory: number
}

/**
 * Show detail page for a specific Accessory
 */
const AccessoryDetail: NextPage<Props> = ({ accessory }) => {
  const pageTitle = `Accessory #${accessory}`

  return (
    <div id="content-container">
      <Head>
        <title>{pageTitle}</title>
        <meta property="og:title" content={pageTitle} />
        <meta property="og:image" content={`${API_SERVER_ROOT}/accessory-image/${accessory}`} />
        <meta property="og:image:type" content="image/png" />
      </Head>
      <div className="text-container">
        <h1 className="hero">Accessory #{accessory}</h1>
        <div style={{ margin: '2rem 0' }}>
          <img
            src={`${API_SERVER_ROOT}/accessory-image/${accessory}`}
            alt={`Accessory #${accessory})`}
            style={{ maxHeight: 350, maxWidth: '95vw', display: 'block', margin: '0 auto' }}
          />
        </div>
        <section className="card">
          <p>
            <strong>Hard Hats Required!</strong> This area of ChainStation Alpha is undergoing some intense
            construction. So, please pardon the dust, and check back a bit later to see what pops up here!
          </p>
        </section>
        <section className="card-notice">
          <p>
            While this site is under construction, details about this Accessory can be accessed{' '}
            <a href={`https://boutique.mooncat.community/a/${accessory}`}>on the MoonCat{ZWS}Community website</a>.
          </p>
        </section>
      </div>
    </div>
  )
}

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  if (typeof ctx.params == 'undefined') {
    console.log('Routing error')
    return { notFound: true }
  }
  const id = ctx.params.id
  if (Array.isArray(id) || typeof id == 'undefined') {
    console.error('Routing error', id)
    return { notFound: true }
  }

  return { props: { accessory: parseInt(id) } }
}

export default AccessoryDetail

import React, { useContext, useEffect, useState } from 'react'
import type { GetServerSideProps, NextPage } from 'next'
import MoonCatGrid from 'components/MoonCatGrid'
import Head from 'next/head'
import Link from 'next/link'
import { useRouter } from 'next/router'
import WarningIndicator from 'components/WarningIndicator'
import {
  API_SERVER_ROOT,
  AppGlobalActionType,
  AppGlobalViewAction,
  AppVisitorContext,
  PREFERENCE_MOONCATVIEWCHANGE_KEY,
  useIsMounted,
} from 'lib/util'
import { MoonCatViewPreference } from 'lib/types'

const ZWS = '\u200B'
const pageTitle = 'MoonCats'

interface Event {
  label: string
  start: string
  end: string
}

const EventBanner = ({ currentEvent }: { currentEvent: Event | null }): JSX.Element | null => {
  const {
    state: { viewPreference },
  } = useContext(AppVisitorContext)

  if (currentEvent == null || viewPreference != 'event') return null
  switch (currentEvent.label.toLowerCase()) {
    case 'anniversary': {
      return (
        <section className="card">
          <h3>Happy Anniversary MoonCats!</h3>
          <p>
            It&rsquo;s party season! The MoonCatRescue Insanely Cute Operation launched August 9, 2017, and so for the
            month of August, the MoonCats are in a partying mood. Take a peek at which MoonCats have anniversary
            celebration accessories from across the six years of this project&rsquo;s existence.
          </p>
        </section>
      )
    }
  }
  return null
}

const MoonCatLookup = () => {
  const [searchId, setSearchId] = useState<string>('')
  const [errorMessage, setErrorMessage] = useState<string>('')
  const router = useRouter()

  function doLookup() {
    setErrorMessage('')
    if (searchId.substring(0, 2) == '0x') {
      // searchId is a MoonCat Hex ID
      if (isNaN(Number(searchId)) || searchId.length != 12) {
        setErrorMessage('Not a valid MoonCat Hex ID')
        return
      }
    } else {
      // searchId is numeric
      const rescueOrder = Number(searchId)
      if (isNaN(rescueOrder)) {
        setErrorMessage('Search term needs to be numeric')
        return
      }
      if (rescueOrder < 0 || rescueOrder >= 25440) {
        setErrorMessage('Not a valid rescue order')
        return
      }
    }

    router.push(`/mooncats/${searchId}`)
  }

  return (
    <div className="mooncat-lookup">
      <p style={{ textAlign: 'center' }}>
        <input
          type="text"
          value={searchId}
          onKeyDown={(e) => {
            if (e.key == 'Enter') doLookup()
          }}
          onChange={(e) => setSearchId(e.target.value)}
          style={{ marginRight: '1em' }}
          placeholder="MoonCat ID"
          autoCorrect="off"
          autoCapitalize="off"
          spellCheck="false"
        />
        <button
          onClick={(e) => {
            e.preventDefault()
            doLookup()
          }}
        >
          Lookup
        </button>
      </p>
      {errorMessage && <WarningIndicator style={{ textAlign: 'center' }} message={errorMessage} />}
    </div>
  )
}

interface Props {
  currentEvent: Event | null
}

const MoonCatsHome: NextPage<Props> = ({ currentEvent }) => {
  const isMounted = useIsMounted()
  const { dispatch } = useContext(AppVisitorContext)

  useEffect(() => {
    if (!isMounted || !dispatch || !currentEvent) return
    const lastViewChange = localStorage.getItem(PREFERENCE_MOONCATVIEWCHANGE_KEY)
    if (lastViewChange == '' || new Date(currentEvent.start) > new Date(Number(lastViewChange))) {
      // Visitor has not changed views while the current event has been active
      dispatch({
        type: AppGlobalActionType.SET_VIEW_PREFERENCE,
        payload: 'event',
      } as AppGlobalViewAction)
    }
  }, [currentEvent, dispatch, isMounted])

  return (
    <div id="content-container">
      <Head>
        <title>{pageTitle}</title>
        <meta property="og:title" content={pageTitle} />
        <meta
          name="description"
          property="og:description"
          content="Browse the entire collection of colorful astral felines"
        />
      </Head>
      <div className="text-container">
        <h1 className="hero">MoonCats</h1>
        <section className="card">
          <p>
            Discovered living on the moon back in 2017, they were <strong>rescued</strong> by many adventurous
            Etherians, and brought onto the blockchain. They now coexist happily with their human Etherian friends,
            serving as <em>companions</em> and <em>guides</em> into the uncharted <strong>Deep Space</strong>.
          </p>
        </section>
        <section className="card-help">
          <p>
            MoonCats are the primary NFT collection of the MoonCat{ZWS}Rescue project. The{' '}
            <a href="https://etherscan.io/address/0x60cd862c9c687a9de49aecdc3a99b74a4fc54ab6">original contract</a> was
            deployed in 2017, before &quot;NFT&quot; was an established term. They were <em>rescued</em>{' '}
            (&quot;minted&quot;) and brought to <em>ChainStation Alpha</em> (the Ethereum mainnet blockchain), and are
            the key characters guiding their human owners through <em>Deep Space</em> (the forward-looking possibilities
            of blockchain/web3 technologies).
          </p>
          <MoonCatLookup />
          <ul className="two-col">
            <li>
              <Link href="/named-mooncats">Browse named MoonCats</Link>
            </li>
          </ul>
        </section>
        {isMounted && <EventBanner currentEvent={currentEvent} />}
      </div>
      <MoonCatGrid moonCats="all" apiPage="mooncats" isEventActive={currentEvent != null} />
    </div>
  )
}

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const request = require('lib/request')

  // Fetch events listing from the API server
  let jsonData = null
  try {
    let rs = await request(`${API_SERVER_ROOT}/events`)
    jsonData = JSON.parse(rs.body.toString('utf8'))
  } catch (err) {
    console.error('Failed to fetch events', err)
  }
  if (!Array.isArray(jsonData)) {
    console.error('Failed to fetch event data', jsonData)
    return { props: { currentEvent: null } }
  }
  const now = new Date()
  const activeEvents = jsonData.filter((e) => {
    return new Date(e.start) < now && new Date(e.end) > now
  })
  if (activeEvents.length == 0) {
    return { props: { currentEvent: null } }
  }
  return { props: { currentEvent: activeEvents[0] } }
}

export default MoonCatsHome

import React, { createContext, useState, useEffect } from 'react'
import { Address } from 'wagmi'
import { MoonCatData, MoonCatFilterSettings, MoonCatViewPreference } from 'lib/types'
import type { NextApiRequest } from 'next'

export const API_SERVER_ROOT = 'https://api.mooncat.community'
export const API2_SERVER_ROOT = 'https://api.mooncatrescue.com'
export const SIWE_LOGIN_STATEMENT = 'Authenticating into the ChainStation web application'
export const USER_SESSION_COLLECTION = 'user-sessions'

export const ADDRESS_DETAILS: { [key: Address]: { type: 'pool' | 'marketplace'; label: string; link: string } } = {
  '0x98968f0747E0A261532cAcC0BE296375F5c08398': {
    type: 'pool',
    label: 'NFTX MOONCAT pool',
    link: 'https://nftx.io/vault/0x98968f0747e0a261532cacc0be296375f5c08398/buy/',
  },
  '0xA8b42C82a628DC43c2c2285205313e5106EA2853': {
    type: 'pool',
    label: 'NFTX MCAT17 pool',
    link: 'https://nftx.io/vault/0xa8b42c82a628dc43c2c2285205313e5106ea2853/buy/',
  },
  '0x67BDcD02705CEcf08Cb296394DB7d6Ed00A496F9': {
    type: 'pool',
    label: 'NFT20 CAT20 pool',
    link: 'https://nft20.io/asset/0x67bdcd02705cecf08cb296394db7d6ed00a496f9',
  },
  '0x7Be8076f4EA4A4AD08075C2508e481d6C946D12b': {
    type: 'marketplace',
    label: 'OpenSea: Wyvern Exchange v1',
    link: 'https://opensea.io/collection/acclimatedmooncats',
  },
  '0x7f268357A8c2552623316e2562D90e642bB538E5': {
    type: 'marketplace',
    label: 'OpenSea: Wyvern Exchange v2',
    link: 'https://opensea.io/collection/acclimatedmooncats',
  },
  '0x00000000006CEE72100D161c57ADA5Bb2be1CA79': {
    type: 'marketplace',
    label: 'Seaport 1.0',
    link: 'https://opensea.io/collection/acclimatedmooncats',
  },
  '0x00000000006c3852cbEf3e08E8dF289169EdE581': {
    type: 'marketplace',
    label: 'Seaport 1.1',
    link: 'https://opensea.io/collection/acclimatedmooncats',
  },
  '0x00000000000006c7676171937C444f6BDe3D6282': {
    type: 'marketplace',
    label: 'Seaport 1.2',
    link: 'https://opensea.io/collection/acclimatedmooncats',
  },
  '0x00000000000001ad428e4906aE43D8F9852d0dD6': {
    type: 'marketplace',
    label: 'Seaport 1.4',
    link: 'https://opensea.io/collection/acclimatedmooncats',
  },
  '0x00000000000000ADc04C56Bf30aC9d3c0aAF14dC': {
    type: 'marketplace',
    label: 'Seaport 1.5',
    link: 'https://opensea.io/collection/acclimatedmooncats',
  },
  '0x59728544B08AB483533076417FbBB2fD0B17CE3a': {
    type: 'marketplace',
    label: 'LooksRare: Exchange',
    link: 'https://looksrare.org/collections/0xc3f733ca98E0daD0386979Eb96fb1722A1A05E69',
  },
  '0x0000000000E655fAe4d56241588680F86E3b2377': {
    type: 'marketplace',
    label: 'LooksRare: Exchange V2',
    link: 'https://looksrare.org/collections/0xc3f733ca98E0daD0386979Eb96fb1722A1A05E69',
  },
  '0x000000000000Ad05Ccc4F10045630fb830B95127': {
    type: 'marketplace',
    label: 'Blur.io: Marketplace',
    link: 'https://blur.io/collection/acclimatedmooncats',
  },
  '0x39da41747a83aeE658334415666f3EF92DD0D541': {
    type: 'marketplace',
    label: 'Blur.io: Marketplace 2',
    link: 'https://blur.io/collection/acclimatedmooncats',
  },
  '0xb2ecfE4E4D61f8790bbb9DE2D1259B9e2410CEA5': {
    type: 'marketplace',
    label: 'Blur.io: Marketplace 3',
    link: 'https://blur.io/collection/acclimatedmooncats',
  },
}

export function sleep(delay: number): Promise<void> {
  return new Promise((resolve) => {
    window.setTimeout(() => {
      resolve()
    }, delay * 1000)
  })
}

/**
 * Given an array of items, add a separator between every one of them.
 * This is a replacement for Array.join() on a list of JSX elements
 */
export function interleave(arr: Array<JSX.Element | string>, glue: string = ', '): Array<JSX.Element | string> {
  let formatted: Array<JSX.Element | string> = []
  arr.forEach((field, index) => {
    formatted.push(field)
    if (index < arr.length - 1) formatted.push(glue)
  })
  return formatted
}

/**
 * Tracking the status of a remote call
 */
type FetchStatus = 'start' | 'pending' | 'done' | 'error'
export function useFetchStatus() {
  return useState<FetchStatus>('start')
}

export interface AppGlobalState {
  verifiedAddresses: {
    status: FetchStatus
    value: Address[]
  }
  viewPreference: MoonCatViewPreference
}

export enum AppGlobalActionType {
  SET_VIEW_PREFERENCE = 'Meow',
  UPDATE_VERIFIED_ADDRESSES = 'I like space!',
  AWARDED_PRIZE = 'Anyone solved the Accessory 572 riddle yet?',
  USE_ATOB = 'SGVsbG8gdGhlcmUh',
}

export interface AppGlobalViewAction {
  type: AppGlobalActionType.SET_VIEW_PREFERENCE
  payload: AppGlobalState['viewPreference']
}
export interface AppGlobalVerifiedAddressesAction {
  type: AppGlobalActionType.UPDATE_VERIFIED_ADDRESSES
  payload: AppGlobalState['verifiedAddresses']
}
export type AppGlobalAction = AppGlobalViewAction | AppGlobalVerifiedAddressesAction

/**
 * Global context for the visitor within the application
 */
export const AppVisitorContext = createContext<{
  state: AppGlobalState
  dispatch: Function | null
}>({
  state: {
    verifiedAddresses: {
      status: 'start',
      value: [],
    },
    viewPreference: null,
  },
  dispatch: null,
})

/**
 * Determine if a the visitor has signed in fully with any addresses
 */
export async function doUserCheck(dispatch: Function) {
  if (dispatch == null) return
  dispatch({
    type: AppGlobalActionType.UPDATE_VERIFIED_ADDRESSES,
    payload: {
      status: 'pending',
      value: [],
    },
  } as AppGlobalVerifiedAddressesAction)
  try {
    let res = await fetch('/api/me')
    let data = await res.json()
    dispatch({
      type: AppGlobalActionType.UPDATE_VERIFIED_ADDRESSES,
      payload: {
        status: 'done',
        value: data.addresses,
      },
    } as AppGlobalVerifiedAddressesAction)
  } catch (err) {
    console.error(err)
    dispatch({
      type: AppGlobalActionType.UPDATE_VERIFIED_ADDRESSES,
      payload: {
        status: 'error',
        value: [],
      },
    } as AppGlobalVerifiedAddressesAction)
  }
}

/**
 * Tracking whether the component has gone through its first reneder or not.
 */
export function useIsMounted() {
  const [mounted, setMounted] = useState(false) // Start as false
  useEffect(() => setMounted(true), []) // On first run, set to true
  return mounted
}

interface FetchResult {
  status: FetchStatus
  data: any
}

/**
 * Custom Hook for fetching an HTTP result, but in an abort-able manner.
 * If the component that uses this hook gets unmounted, an AbortController is
 * used to stop the HTTP request in-flight
 */
export function useDelayedFetch(fetchURI: string) {
  const [fetchResult, setFetchResult] = useState<FetchResult>({ status: 'start', data: null })
  useEffect(() => {
    console.log('custom hook mount')
    const controller = new AbortController()
    const signal = controller.signal

    let handle = window.setTimeout(async () => {
      setFetchResult({ status: 'pending', data: null })
      let rs = await fetch(fetchURI, { signal })
      let data = rs.ok ? await rs.json() : null
      setFetchResult({ status: rs.ok ? 'done' : 'error', data })
    })
    return () => {
      console.log('custom hook unmount')
      controller.abort() // Stop fetch in-progress, if it exists
      window.clearTimeout(handle) // Clear any timeout handler, if it exists
    }
  }, [fetchURI])
  return fetchResult
}

export const PREFERENCE_MOONCATVIEW_KEY = 'mooncatview'
export const PREFERENCE_MOONCATVIEWCHANGE_KEY = 'mooncatviewchange'
export function getViewPreference(): MoonCatViewPreference {
  if (typeof window == 'undefined') return 'accessorized'
  const params = new URLSearchParams(window.location.search)

  // Set MoonCat view style preference
  const queryPreference = params.get('view')
  if (
    queryPreference !== null &&
    ['event', 'accessorized', 'mooncat', 'face'].includes(queryPreference.toLowerCase())
  ) {
    // Query parameter takes priority
    return queryPreference as MoonCatViewPreference
  } else {
    // Check local storage
    let savedPreference = localStorage.getItem(PREFERENCE_MOONCATVIEW_KEY) as MoonCatViewPreference
    if (savedPreference != null) {
      console.debug('Setting view preference:', savedPreference)
      return savedPreference
    } else {
      console.debug('Setting default view preference')
      return 'accessorized'
    }
  }
}

const filterEnumProps: string[] = ['classification', 'hue', 'pale', 'facing', 'expression', 'pattern', 'pose', 'named']

/**
 * Type predicate for MoonCatFilterSettings
 *
 * https://www.typescriptlang.org/docs/handbook/2/narrowing.html#using-type-predicates
 * Checks a bare object to ensure it follows the Type of a MoonCatFilterSettings object,
 * so it can be cast to it safely with Typescript after that point.
 */
function validateFilterSetings(arg: any): arg is MoonCatFilterSettings {
  if (typeof arg != 'object') return false

  for (let i = 0; i < filterEnumProps.length; i++) {
    if (typeof arg[filterEnumProps[i]] != 'undefined') {
      if (typeof arg[filterEnumProps[i]] != 'string') return false
      if (arg[filterEnumProps[i]].toLowerCase() != arg[filterEnumProps[i]]) return false
    }
  }
  if (typeof arg.nameKeyword != 'undefined' && typeof arg.nameKeyword != 'string') return false
  if (typeof arg.rescueYear != 'undefined' && typeof arg.rescueYear != 'number') return false

  return true
}

export function queryToFilterSettings(query: NextApiRequest['query']): MoonCatFilterSettings | null {
  const filters: any = {}
  for (let prop of filterEnumProps) {
    if (typeof query[prop] != 'undefined' && !Array.isArray(query[prop])) {
      filters[prop] = query[prop]
    }
  }
  if (typeof query.nameKeyword != 'undefined' && !Array.isArray(query.nameKeyword)) {
    filters.nameKeyword = query.nameKeyword
  }
  if (typeof query.rescueYear != 'undefined' && !Array.isArray(query.rescueYear)) {
    filters.rescueYear = parseInt(query.rescueYear)
  }
  if (!validateFilterSetings(filters)) return null
  return filters
}

/**
 * Hue colors that are exactly in the middle of the range defined for that color name
 *
 * Each color range has two integers because hue values are truncated, so the degree
 * just below the exact value is also within one degree of that value.
 */
const trueHues: number[] = [
  359,
  0, // Red
  29,
  30, // Orange
  59,
  60, // Yellow
  89,
  90, // Chartreuse
  119,
  120, // Green
  149,
  150, // Teal
  179,
  180, // Cyan
  209,
  210, // SkyBlue
  239,
  240, // Blue
  269,
  270, // Purple
  299,
  300, // Magenta
  239,
  330, // Fuchsia
]

/**
 * Mapping for hue values that are on the edge of another color.
 *
 * The value of this mapping is the other color that it's close to.
 */
const edgeHues: { [key: number]: string } = {
  15: 'Orange', // is Red
  16: 'Red', // is Orange
  45: 'Yellow', // is Orange
  46: 'Orange', // is Yellow
  75: 'Chartreuse', // is Yellow
  76: 'Yellow', // is Chartreuse
  105: 'Green', // is Chartreuse
  106: 'Chartreuse', // is Green
  135: 'Teal', // is Green
  136: 'Green', // is Teal
  165: 'Cyan', // is Teal
  166: 'Teal', // is Cyan
  195: 'SkyBlue', // is Cyan
  196: 'Cyan', // is SkyBlue
  225: 'Blue', // is SkyBlue
  226: 'SkyBlue', // is Blue
  255: 'Purple', // is Blue
  256: 'Blue', // is Purple
  285: 'Magenta', // is Purple
  286: 'Purple', // is Magenta
  315: 'Fuchsia', // is Magenta
  316: 'Magenta', // is Fuchsia
  345: 'Red', // is Fuchsia
  346: 'Fuchsia', // is Red
}

function yearStart(mc: MoonCatData) {
  switch (mc.rescueOrder) {
    case 0:
      return '2017'
    case 3365:
      return '2018'
    case 5684:
      return '2019'
    case 5755:
      return '2020'
    case 5758:
      return '2021'
  }
  return false
}

function yearEnd(mc: MoonCatData) {
  switch (mc.rescueOrder) {
    case 3364:
      return '2017'
    case 5683:
      return '2018'
    case 5754:
      return '2019'
    case 5757:
      return '2020'
    case 25439:
      return '2021'
  }
  return false
}

export function getMoonCatTidbits(moonCat: MoonCatData): { [key: string]: false | true | string } {
  let bytes = []
  for (let i = 2; i < moonCat.catId.length; i += 2) {
    bytes.push(parseInt(moonCat.catId.slice(i, i + 2), 16))
  }

  const hexRepeats = moonCat.catId.slice(2).match(/([a-f0-9])\1{2,}/gi)
  const glowSum = bytes[2] + bytes[3] + bytes[4]

  return {
    numericId: /^[0-9]+$/.test(moonCat.catId.slice(2)),
    alphaId: /^[a-f]+$/i.test(moonCat.catId.slice(4)),
    repeatId: hexRepeats == null ? false : hexRepeats.join(', '),
    firstDay: moonCat.rescueOrder <= 491,
    firstWeek: moonCat.rescueOrder <= 1568,
    yearStart: yearStart(moonCat),
    yearEnd: yearEnd(moonCat),
    trueColor: trueHues.includes(moonCat.hueInt),
    edgeColor: typeof edgeHues[moonCat.hueInt] == 'undefined' ? false : edgeHues[moonCat.hueInt],
    darkGlow: glowSum < 180 ? `${glowSum}/765 brightness` : false,
    brightGlow: glowSum > 585 ? `${glowSum}/765 brightness` : false,
  }
}

export function filterMoonCatList(mooncats: MoonCatData[], filters: MoonCatFilterSettings) {
  return mooncats.filter((moonCat) => {
    if (typeof filters.rescueYear != 'undefined') {
      if (filters.rescueYear != moonCat.rescueYear) return false
    }
    if (typeof filters.classification != 'undefined') {
      if (filters.classification == 'genesis' && typeof moonCat.genesis == 'undefined') return false
      if (filters.classification == 'rescue' && moonCat.genesis == true) return false
    }
    if (typeof filters.hue != 'undefined') {
      if (filters.hue != moonCat.hueName) return false
    }
    if (typeof filters.pale != 'undefined') {
      switch (filters.pale) {
        case 'no':
          if (moonCat.pale === true) return false
          break
        case 'yes':
          if (typeof moonCat.pale == 'undefined' || moonCat.pale === false) return false
          break
      }
    }
    if (typeof filters.facing != 'undefined') {
      if (filters.facing != moonCat.facing) return false
    }
    if (typeof filters.expression != 'undefined') {
      if (filters.expression != moonCat.expression) return false
    }
    if (typeof filters.pattern != 'undefined') {
      if (filters.pattern != moonCat.pattern) return false
    }
    if (typeof filters.pose != 'undefined') {
      if (filters.pose != moonCat.pose) return false
    }
    if (typeof filters.named != 'undefined') {
      switch (filters.named) {
        case 'no':
          if (typeof moonCat.nameRaw != 'undefined') return false
          break
        case 'yes':
          if (typeof moonCat.nameRaw == 'undefined') return false
          break
        case 'valid':
          if (typeof moonCat.nameRaw == 'undefined') return false
          if (moonCat.name === true) return false
          break
        case 'invalid':
          if (typeof moonCat.nameRaw == 'undefined') return false
          if (moonCat.name !== true) return false
          break
      }
    }
    if (typeof filters.nameKeyword != 'undefined') {
      if (typeof moonCat.nameRaw == 'undefined') return false // Has no name
      if (filters.nameKeyword.substring(0, 2) == '0x') {
        // Search the raw name
        if (moonCat.nameRaw.substring(2).indexOf(filters.nameKeyword.substring(2).toLowerCase()) < 0) return false
      } else {
        // Search the parsed name
        if (moonCat.name === true) return false
        if (moonCat.name!.toLowerCase().indexOf(filters.nameKeyword.toLowerCase()) < 0) return false
      }
    }
    return true
  })
}

import { getApps, initializeApp } from 'firebase-admin/app'
import { getFirestore } from 'firebase-admin/firestore'
import { SiweMessage, SiweResponse } from 'siwe'
import { USER_SESSION_COLLECTION } from 'lib/util'

// Use the Firebase Admin SDK to access Firestore.
export function getAppFirestore(): FirebaseFirestore.Firestore {
  const existingApps = getApps()
  if (existingApps.length > 0) {
    return getFirestore(existingApps[0])
  } else {
    initializeApp()
    const db = getFirestore()
    db.settings({ ignoreUndefinedProperties: true })
    return db
  }
}

export async function isSessionValid(userSiwe: Partial<SiweMessage>, signature: string): Promise<boolean> {
  const db = getAppFirestore()
  const siwe = new SiweMessage(userSiwe)

  // Get the expected nonce from the database, not the user session
  const dbSession = await db.collection(USER_SESSION_COLLECTION).doc(`${siwe.address}-${siwe.chainId}`).get()
  if (!dbSession.exists) {
    // No saved session on the server-side
    return false
  }
  const expectedNonce = dbSession.get('nonce')
  if (!expectedNonce) {
    console.error('Corrupted user session in database', dbSession.data())
    return false
  }

  // Verify the SIWE message is still valid (nonce as expected, not expired, etc.)
  let verifyRes: SiweResponse
  try {
    verifyRes = await siwe.verify({
      signature,
      nonce: expectedNonce,
    })
  } catch (err) {
    console.error('SIWE error', (err as SiweResponse).error)
    return false
  }

  return true
}
